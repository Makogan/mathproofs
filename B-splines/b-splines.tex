\documentclass{article}

\usepackage[margin=1cm,paperwidth=20cm,paperheight=265cm]{geometry}
\usepackage{amsmath}
\usepackage{standalone}
\usepackage{luamplib}
\mplibnumbersystem{double}
\usepackage{tikz,pgf}
\usepackage{mathtools}
\pagenumbering{gobble}
\usepackage{enumitem}  % http://www.ctan.org/pkg/enumitem
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{math}
\mplibtextextlabel{enable}
\setlength{\parindent}{0pt}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{xcolor}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\begin{document}
\noindent
\section*{Definition and intuition of the Spline Basis Functions}

B-splines are a useful building block for many geometric algorithms, this document will try to give a good intuition to their properties and construction.
\\ \\
The mathematical definition of a B-spline contains 3 essential components. 

\begin{itemize}
	\item A set of control points $C$ which can be defined in any dimension. 
	\item A knot sequence $K$. An ordered finite set set of $|C| + n$ scalar values, where $n$ is the order of the spline.
	\item A set of recursively defined basis functions $B$, which depend on the knot values.
\end{itemize}

The recursive definition of a basis function $B_{i,l}$, called the $\emph i^{th}$ spline of order $l+1$ is as follows:

\begin{align*}
B_{i,0}(t) &= 
\begin{cases} 
	1 &  k_{i}\leq t < k_{i+1} \\
	0 & \text{otherwise} \\
\end{cases} \\
B_{i,l}(t) &= \frac{t - k_i}{k_{i+l} - k_i}B_{i,l-1}(t) + \frac{k_{i+l+1} - t}{k_{i+l+1} - k_{i+1}}B_{i+1,l-1}(t)&
\end{align*}

What is important to note here, is that the basis function $B_{i,0}$ depends on exactly 2 knots, $k_i$ and $k_{i+1}$, $B_{i,1}$ depends on $k_i, k_{i+1}, k_{i+2}$ and so on. One can visualize the way high order B splines depend on lower order ones with the following diagram:

\begin{figure}[h]
	\centering
	\scalebox{0.45}{\input{spline_extras/basis_lattice.tex}}
\end{figure}

By a simple induction proof (omitted), we can show that the basis $B_{i,l}$ depends on the knot values $[k_i, \dots, k_{i+l+1}]$.

The neighbours of each spline share most  of their knots except for one, and that the splines $B_{i, l}$ and $B_{i+1,l}$ share the knots $[k_{i+1},k_{i+l+1}]$. For example in the following figure we can see how knot values are shared between the last and second to last splines.
\vspace{0.5cm}
\begin{figure}[h]
	\centering
	\scalebox{0.35}{\input{spline_extras/spline_set.tex}}
\end{figure}
\vspace{0.5cm}

Given a fixed value $t$, only a single order 1 basis function $B_{i,0}$ is non zero, by definition. Since there's only 2 basis functions of order 2, $B_{i,1}, B_{i+1,1}$, that depend on that specific basis function, only those 2 can be non zero, and for the same reason, only 3 basis functions of order 3, $B_{i,2}, B_{i+1,2}, B_{i+2,2}$, are non zero... So in general, for a spline of order $l$ only $l$ basis functions can be non zero.

An important property of those non zero basis functions, is that they must add to 1. Which we will prove by the following induction proof.

\section*{B splines are an affine combination}

In general, for $m+1$ basis functions of order $l+1$ such that $m+1\geq l+1$, it is true that $\sum_{j=0}^{m} B_{j,l}(t) = 1$.

\begin{itemize}
\item
\textbf{Base case, splines of order 1} \\ \\
Assume we have a set of $m+1$ basis functions of order 1 that includes $B_{i,0}$. Where $B_{i,0}$ is the basis that is not zero over the interval $[k_i, k_{i+1}]$. Fix $t \in [k_i, k_{i+1}]$. 
Then:
 $$\sum_{j=0}^{m} B_{j,0}(t) = 1$$

Trivially since $B_{i,0}=1$ by construction and all other basis are 0.


\item
\textbf{Inductive step, splines of order $l>1$ \\ \\}
Assume we have a set of $m+2$ basis functions of order $l-1$ such that  $\sum_{j=0}^{m+1} B_{j,l-1}(t) = 1$ and $m+1 \geq l+1$. 

Take the set of $m+1$ basis of order $l$ that are defined from the basis of order $l-1$. The sum of these splines is:

$$\sum_{j=0}^{m} B_{j,l}(t) = \sum_{j=0}^{m} \frac{t-k_j}{k_{j+l} - k_j}B_{j,l-1}(t) + \frac{k_{j+l+1}-t}{k_{j+l+1}-k_{j+1}}B_{j+1,l-1}(t)$$

By definition. 

And this sum can be rewritten as:

\begin{align*}
\frac{t-k_0}{k_{l} - k_0}B_{0,l-1}(t) + 
\Bigg(&\sum_{j=1}^{m} \frac{k_{j+l}-t}{k_{j+l}-k_{j}}B_{j,l-1}(t) + \frac{t-k_j}{k_{j+l} - k_j}B_{j,l-1}(t)\Bigg) +
 \frac{k_{m+1}-t}{k_{m+l+1}-k_{m+1}}B_{m+1,l-1}(t)\\
 %
&\sum_{j=1}^{m}  \Bigg(\frac{k_{j+l}-t}{k_{j+l}-k_{j}} + \frac{t-k_j}{k_{j+l} - k_j}\Bigg)B_{j,l-1}(t)\\
%
&\sum_{j=1}^{m}  \Bigg(\frac{k_{j+l}-k_j}{k_{j+l}-k_{j}}\Bigg)B_{j,l-1}(t)\\
&\sum_{j=1}^{m}B_{j,l-1}(t)
\end{align*}

By a prior claim, we know that exactly $l-1 \leq m - 2$ basis functions of order $l-1$ are non zero. WLOG let's assume that the non zero basis functions belong to the set $\{B_{1,l-1},\dots, B_{m, l-1}\}$. Since $\sum_{j=0}^{m+1} B_{j,l-1}(t) = 1$ yet the first and last element of that sum are 0 by our previous assumption, it must be that $\sum_{j=1}^{m}B_{j,l-1}(t) = 1$ and thus:

\begin{align*}
&\frac{t-k_0}{k_{l} - k_0}B_{0,l-1}(t) = 0\\
&\sum_{j=1}^{m}B_{j,l-1}(t) = 1\\
&\frac{k_{m+1}-t}{k_{m+l+1}-k_{m+1}}B_{m+1,l-1}(t) = 0
\end{align*}

Then:

$$\sum_{j=0}^{m} B_{j,l}(t) = 1$$

\textbf{Conclusion \\ \\}
It is thus the case that if a basis set of order $l-1$ adds to 1, the same is true of the dependent basis set of order $l$ and thus the property holds for all basis function orders.
\end{itemize}

Some may be skeptical as to whether that WLOG step is mathematically correct. In other words, I will show why we can ignore the first and last element of that sum. Let's look at the following diagram:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.35}{\input{spline_activations/spline_activations0.tex}}
\end{figure}
\vspace{0.5cm}

We can see that only 1 basis function of the 11 basis of order 1 is non zero. And we also see that only 2 basis of order 2 is non zero. In this diagram the only non zero functions are those that are connected to $B{i,0}$ through a red edge.

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.35}{\input{spline_activations/spline_activations1.tex}}
\end{figure}
\vspace{0.5cm}

On the next level, there are 3 non zero functions, once again, connected by a red edge to the non zero functions of lower order. Then if we go all the way up to the last level:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.35}{\input{spline_activations/spline_activations4.tex}}
\end{figure}
\vspace{0.5cm}

Notice how in all of the diagrams, the first and last basis functions always have a 0 term, the left term for the first basis, and the right term for the last. It's even easier to see this if we label the diagram:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.35}{\input{spline_activations/spline_activations_labels.tex}}
\end{figure}
\vspace{0.5cm}

In this specific example, the left term of $B_{i,5}$, in other words, $B_{i,4}$ is 0, the right term of $B_{i+5,5}$, namely $B_{i+6,4}$ is also 0. From that diagram it is evident that although $l$ non zero basis functions depend on $l+1$ basis functions of lower order, there's only $l-1$ non zero basis of that lower order; we can remove the first and the last terms of our sum, since they will always be 0. And thus our WLOG statement in the inductive proof is acceptable.

\section*{Properties of the basis functions}

The most fundamental basis functions are those of order 1, $B_{i,0}$, as all other basis functions depend on them. These are essentially "switches" as they are 1 over a small interval and 0 everywhere else on the domain of the function. This means that when we multiply the control values (or control points) with the basis and add them all together, only a single value can possibly be non 0. 
\\ \\
Say we are given a spline standard basis (we will explain this term later on) with 10 control points. Then the 10 spline functions of order 0 look as follows:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.5}{
		\begin{mplibcode}
		input metapost/spline_basis.mp;
		% Start figure
		beginfig(0);
		plot_all_basis(10, 0);
		endfig;
		\end{mplibcode}
	}
\end{figure}
\vspace{0.5cm}

Each of the basis of order 0 can then be combined with the immediately following one to create 10 basis of order 1:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.5}{
		\begin{mplibcode}
		input metapost/spline_basis.mp;
		% Start figure
		beginfig(0);
		plot_all_basis(10, 1);
		endfig;
		\end{mplibcode}
	}
\end{figure}
\vspace{0.5cm}

Three things should become apparent, first, at any point, the functions add to 1, as we previously showed, but also, each basis can be split into an increasing section and a decreasing one. Indeed this is how local support works, each basis (and by consequence its associated control point) can only affect the curve in a small interval; outside of that interval, it has no effect on the shape of the curve. The final thing is, the first and last curve are slightly different from all the other ones, they are the only monotonous functions in the entire set. This property will become more obvious for higher order basis so let's focus on the basis of order 3:

\vspace{0.5cm}
\begin{figure}[H]
	\centering
	\scalebox{0.5}{
		\begin{mplibcode}
		input metapost/spline_basis.mp;
		% Start figure
		beginfig(0);
		plot_all_basis(10, 2);
		endfig;
		\end{mplibcode}
	}
\end{figure}
\vspace{0.5cm}

We can split the above curves into 2 sets. First, the functions from $B_{2,3}$ to $B_{7,3}$ will be called the interior basis. Second, the functions $B_{0,3}$, $B_{1,3}$, $B_{8,3}$ and $B_{9,3}$ will be called the boundary basis.

The number of boundary basis should always be equal to the degree (the order minus one) of the spline. And the number of interior basis is thus equal to $n - 2l$ where $n$ is the number of control points and $l$ the degree. We can look at the splines of order 4 to verify that this holds for that order as well:

 \vspace{0.5cm}
 \begin{figure}[H]
 	\centering
 	\scalebox{0.5}{
 		\begin{mplibcode}
 		input metapost/spline_basis.mp;
 		% Start figure
 		beginfig(0);
 		plot_all_basis(10, 3);
 		endfig;
 		\end{mplibcode}
 	}
 \end{figure}
 \vspace{0.5cm}

All the properties we previously mentioned are still present here, so it seems true, but does it hold in general?

\subsection*{Knot sequence}

Before being able to answer that question we need to talk about the knot sequence. Although the knot sequence can be any arbitrary vector of scalar values that obeys the property $k_i \leq k_{i+1}$ the above curves were generated with a standard knot sequence. A standard knot sequence is a knot sequence where each value is separated by a constant offset except for a set of boundary knot values that are repeated at the start and at the end a number of times equal to the order of the curve. Let's say we have 10 control points such as with the prior examples. The standard knot sequences of the first 4 orders of the spline with those 10 points are:
\begin{itemize}
 	\item order 1: $$[0,1,2,3,4,5,6,7,8,9,10]$$
  	\item order 2: $$[0,0,1,2,3,4,5,6,7,8,9,9]$$
  	\item order 3: $$[0,0,0,1,2,3,4,5,6,7,8,8,8]$$
  	\item order 3: $$[0,0,0,0,1,2,3,4,5,6,7,7,7,7]$$
\end{itemize}

In general for a spline with a set of control points $C$ and order $m$ the total number of knots $n$ should be $n = |C| + m$. If we define the degree $d$ of the curve as $d = m-1$ and define some constant $\delta > 0$ offset between knots then the knot vector must be defined as:

\begin{align*}
k_i &= 
\begin{cases} 
	0 &  \text{if } i < m \\
	(i-d) \cdot\delta & \text{if } m \leq i \leq |C| \\
	(|C| - d) \cdot\delta & \text{if } |C| < i\\
\end{cases} \\
\end{align*}

Since delta can be any positive number, this would also be an appropriate and equivalent standard knot sequence for a spline with 10 points of order 2: $[0,0,\frac{1}{9},\frac{2}{9},\frac{3}{9},\frac{4}{9},\frac{5}{9},\frac{6}{9},\frac{7}{9},\frac{8}{9},1,1]$.

Something that we should notice from this is that one of the properties we noticed in the curves has an analogous property in the knot vector. The number of boundary values is 2*$m$ and we will see this is not a coincidence.

\subsection*{First boundary basis properties}
Let's recall the recursive definition:

\begin{align*}
B_{i,0}(t) &= 
\begin{cases} 
	1 &  k_{i}\leq t < k_{i+1} \\
	0 & \text{otherwise} \\
\end{cases} \\
B_{i,l}(t) &= \frac{t - k_i}{k_{i+l} - k_i}B_{i,l-1}(t) + \frac{k_{i+l+1} - t}{k_{i+l+1} - k_{i+1}}B_{i+1,l-1}(t)&
\end{align*}

Let's assume a standard knot sequence for a spline of order 2 and that $t=0$. Then $B_{0,0}(t) = 0$, since $k_0 = 0$ and $k_1=0$; and $B_{1,0}(t) = 1$ since $k_1 = 0$ and $k_2$ = $\delta$. This implies that $B_{0,1}(t) = \frac{k_{2} - t}{k_2 - k_1}$ over the range $[0, \delta)$ which is clearly a decreasing function.
\\ \\
More generally, let's assume we have a spline of order $n$ and degree $d=n-1$ with a standard knot sequence. Assume $t \in [0, \delta)$. Then $B_{d-i, j}(t) = 0$ for $0 < i < j$ and

 $$B_{d-i, i}(t) = \prod^{i}_{j=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}$$.
\begin{itemize}
\item \textbf{Base case, basis of degree 0} \\

Given the definition of the sequence, the only non zero basis of order 0 is $B_{d,0}$. Since for: 

$i < d$; $k_i \leq k_{i+1} \leq t \implies B_{i,0}(t)=0$ and for $i > d$; $t < k_{d+1} \leq k_{i} \implies B_{i, 0}(t) = 0$
\\ \\
Since all other basis of degree 0 are 0 at that $t$, the following is trivial:

$$B_{d-1,1}(t) = \frac{k_{d+1}-t}{k_{d+1}-k_{d}} \cdot B_{d, 0}(t) = \prod^1_{i=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}$$

\item \textbf{Inductive case, basis of higher degree $j > 0$} \\

Assume that for the defined spline and for $t \in [0, \delta)$ it is true that $B_{i, j-1}(t) = 0$ for $i<d-j$, and that $$B_{d-(j-1),j-1}(t)=\prod^{j-1}_{i=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}$$

With all other basis of the same degree being 0.

By definition:
$$B_{d-j, j}(t) = \frac{t-k_{d-j}}{k_{d} - k_{d-j}}B_{d-j, j-1}(t) + \frac{k_{d+1}-t}{k_{d+1}-k_{d-j+1}}B_{d-j+1, j-1}(t)$$

The left hand side is 0, and so:
$$B_{d-j, j}(t) = 0 + \frac{k_{d+1}-t}{k_{d+1}-k_{d-j+1}}\bigg(\prod^{j-1}_{i=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}\bigg) = \prod^{j}_{i=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}$$

Since all other basis of degree $j-1$ are 0, we can trivially conclude that all other basis of degree $j$ are also 0.
\end{itemize}

\textbf{Conclusion \\ \\}
Thus we have successfully proven the property that for $i>0$: 

$$B_{d-i, i}(t) = \prod^{i}_{j=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}$$

The above holds for any arbitrary order.

Let's analyze the variations of $B_{0,d}(t)$ over $(0,\delta)$ using derivatives. And prove that it's a strictly decreasing function:

\begin{itemize}
\item \textbf{Base case, basis of degree 1} \\

The first derivative of the spline gives:

$$B_{d-1,1}(t) = \frac{k_{d+1}-t}{k_{d+1} - k_{d}} \implies B'_{d-1,1}(t) = \frac{-t}{k_{d+1}-k_d}$$

Which is strictly negative over the chosen domain.

\item \textbf{Inductive case, basis of degree $i > 0$} \\

Assume $B_{d-(j-1),(j-1)}(t)$ is a strictly decreasing function, or in other words that its derivative is negative. Then, using $K$ to denote a constant, positive, knot delta:

\begin{align*}
B_{d-j,j}'(t) &= \frac{-t}{K}B_{d-(j-1), j-1}(t) + \frac{k_{d+1}-t}{K}B_{d-(j-1), j-1}'(t)\\
	&= \frac{-t}{K}\bigg(\prod^{i}_{j=1}\frac{k_{d+1} - t}{k_{d+1}-k_{d-i + 1}}\bigg) + \frac{k_{d+1}-t}{K}B_{d-(j-1), j-1}'(t)
\end{align*}

The product inside the brackets is clearly positive so the left component of the sum is negative. By our prior assumption $B_{d-(j-1), j-1}'(t)$ is negative; and $(k_{d+1} - t)$ is positive by construction, so the derivative is strictly negative and as such the function is monotonously decreasing over the interval $(0, \delta)$.

\end{itemize}

\textbf{Conclusion} \\

The basis $B_{d-i, i}(j)$ are decreasing over the interval $(0, \delta)$.

This provides us with a perfect explanation as to why the first function in the plotted graphs we saw before behaves so differently from the others; and of course, since the proofs for the last basis are very similar, this also explains why the last function is monotonously increasing.

\subsection*{Higher boundary and interior basis}

The rest of the basis functions are not that different from the first and last. Superficially, we can see how the second basis is built by traveling along the lattice. Let's use green to denote non zero edges in the dependency graph and black edges denote zero valued edges. Let's assume $t \in (0,\delta)$ then the dependency graph of the basis $B_{i+1,5}(t)$ with $i=0$ looks as follows:

\begin{figure}[H]
 	\centering
 	\scalebox{0.35}{
		\begin{mplibcode}
		input metapost/lattice.mp;
		u:=2cm;
		
		% Start figure
		beginfig(0);
		
		for i=1 upto 6:
			lattice(i,4,1, black);
		endfor;
		
	    draw (3,-1)*u--(7,-5)*u withpen pencircle scaled 5bp withcolor green;
	    draw (3,-1)*u--(4,0)*u withpen pencircle scaled 5bp withcolor green;
	    draw (4,0)*u--(8,-4)*u withpen pencircle scaled 5bp withcolor green;
	    draw (8,-4)*u--(7,-5)*u withpen pencircle scaled 5bp withcolor green;
	    draw (7,-3)*u--(6,-4)*u withpen pencircle scaled 5bp withcolor green;
	   	draw (6,-2)*u--(5,-3)*u withpen pencircle scaled 5bp withcolor green;
	   	draw (5,-1)*u--(4,-2)*u withpen pencircle scaled 5bp withcolor green;
		
		low_basis_labels(10);
		high_basis_labels(5);
		endfig;
		\end{mplibcode}
	}
\end{figure}

Notice how the left term of of that sequence is almost identical to the right term of the basis $B_{i,0}$ except for the last product and how the right term is composed of sums and products dependent only on the values of the basis $B_{i,i}(t)$. In general terms, as we move towards the interior of the basis space (basis with higher indices). More and more basis become non zero until we reach a regular, rectangular, structure that repeats itself for all interior curves.

\begin{figure}[H]
 	\centering
 	\scalebox{0.35}{
		\begin{mplibcode}
		input metapost/lattice.mp;
		u:=2cm;
		
		% Start figure
		beginfig(0);
		
		for i=1 upto 6:
			lattice(i,4,1, black);
		endfor;
		
		lattice(4,1,4, green);	
		inverted_lattice_voffset(3.5,1,4, green, -3);
		lattice_voffset(3.5,1,4, green, -1);
		inverted_lattice_voffset(4.5,0,4, green, -2);
		
		low_basis_labels(10);
		high_basis_labels(5);
		endfig;
		\end{mplibcode}
	}
\end{figure}

\begin{figure}[H]
 	\centering
 	\scalebox{0.35}{
		\begin{mplibcode}
		input metapost/lattice.mp;
		u:=2cm;
		
		% Start figure
		beginfig(0);
		
		for i=1 upto 6:
			lattice(i,4,1, black);
		endfor;
		
		lattice(3,1,4, green);	
		inverted_lattice_voffset(3.5,1,4, green, -3);
		lattice_voffset(3.5,1,4, green, -1);
		inverted_lattice_voffset(2.5,0,4, green, -2);
		
		low_basis_labels(10);
		high_basis_labels(5);
		endfig;
		\end{mplibcode}
	}
\end{figure}

In the above diagrams we can observe the non zero dependency connections of 2 basis towards the interior of basis space.

This structure, combined with the changes in the knots are what creates the visual changes in the shapes of the curves in the previous graphs.

\section*{De Boor's algorithm}

DeBoor's Algorithm is a generalisation of DeCastlejeau's algorithm for Bezier curves. It also involves a succession of linear interpolations to generate the final curve point. Let's use the notation $K_{i,j}$ to denote the knot interval $[k_i, k_{j}]$, with $i\leq j$. For example, for a given value of $t$, if $t$ is in the interval $K_{i, i+1}$, then $k_i \leq t < k_{i+1}$. \\

\begin{wrapfigure}[39]{L}[0pt]{0.5\linewidth}
	 \includegraphics[width=\linewidth]{images/order2.png} 
	 \includegraphics[width=\linewidth]{images/order3.png} 
	 \includegraphics[width=\linewidth]{images/order4.png} 
\end{wrapfigure}

Knowing $t$, the degree $d$ of the curve and the ordered set of control points $C=\{c_i\}$ DeBoor's algorithm operates as follows:

\begin{itemize}
	\item Find the interval such that $t \in K_{i,i+1}$. 
	
	\item Find the $d+1$ control points from $c_i$ to $c_{i+d}$. 
	
	\item Find the knots from $k_{i-d}$ to $k_{i+d}$.
	
	\item Linearly interpolate from $c_i$ to $c_{i+1}$ by $\frac{t-k_{i-d+1}}{k_i - k_{i-d+1}}$ to generate the point $c^1_i$.
	
	\item Repeat the above interpolation for all $c_j$ where $i < j < c_{i+d}$ by a factor of $\frac{t-k_{j-d+1}}{k_j-k_{j-d+1}}$ to generate the set $C^1=\{c^1_i\}$ and notice that $|C| = |C^1|+1$.
	
	\item Consider this set to have degree $d-1$ and repeat the interpolation once more. So for example $c^1_i, c^1_{i+1}$ get interpolated by a factor of $\frac{t-k_{i-d+2}}{k_i - k_{i-d+2}}$
	
	\item repeat until we calculate a single point. That point is the point on the curve.
\end{itemize}

Indeed, if you look at the 3 figures on the left, which represent a spline of degree 1, a spline of degree 2 and a spline of degree 3 from top to bottom, you can see how DeBoor's algorithm works geometrically.
\\ \\
A spline of degree 1 is nothing but a linear interpolation of the control points. A spline of degree 2 is generated by computing 2 linear interpolations (marked in yellow) to create 2 intermediary points, that then get interpolated once more (marked in green) to generate the final point.
\\ \\
For the spline of degree 3, we start with 4 points, compute 3 intermediary ones, then compute 2 intermediary ones, then compute a final interpolation to generate the curve point. 
\\ \\
So DeBoor's and DeCastlejeau's algorithms are identical, save for the fact that DeBoor's may use different coefficients during the interpolation, and that DeBoor's doesn't use every single point in the spline but a finite set. Indeed, DeCastlejeau's is actually just a specific instance of DeBoor's.
\\ \\
DeBoor's algorithm can be programmed in a time and memory efficient way by using a constant size memory array to cache the intermediary results of the interpolation. 

A python implementation may look like this:

\lstset{style=mystyle}
\begin{lstlisting}[language=Python]
# Knot vector, control points of the curve, t, spline order
def p_curve(knots, points, t, order=3):
    degree = order - 1

    if len(points) <= degree:
        return points[0]

    # Find the index of the interval containing t
    index = 0
    while not(t>= knots[index] and t < knots[index+1]):
        index += 1

    # Initialize a list with an 'order' number of slots
    control_points = [points[0]] * order
    # Copy the active control points which go from (index-degree) to (index)
    # These are the control points of degree 0
    for i in range(0, order):
        control_points[i] = points[index - degree + i]

    # Generate the intermediary control points, from lowest order to highest
    for l in range(0, degree):
        # Iterate over the control points from back to front
        # (only (degree - current_order) control points are needed to compute the next level)
        for j in range(degree, l, -1):
            alpha = (t - knots[index - degree + j]) / (knots[index - l + j] - knots[index - degree + j])
            control_points[j] = (1.0-alpha) * control_points[j-1] + alpha * control_points[j]

    return control_points[degree]
\end{lstlisting}

Let's convince ourselves that this is correct. Let's recall from the proof that all basis functions add to 1 that for the basis of degree $d$:

$$\sum^m_{j=0} B_{j,d}(t) = \sum_{j=1}^{m} \bigg(\frac{k_{j+d}-t}{k_{j+d}-k_{j}}B_{j,d-1}(t) + \frac{t-k_j}{k_{j+d} - k_j}B_{j,d-1}(t)\bigg)$$

If the order is $d+1$, then only $d$ basis of order $d$ are non zero, as we previously mentioned on that proof. assuming $s$ is the index of the first non zero basis of lower order, we get:

$$\sum^{m}_{j=0}B_{i,d}(t) = \sum_{j=s}^{s+d}  \Bigg(\frac{k_{j+d}-t}{k_{j+d}-k_{j}} + \frac{t-k_j}{k_{j+d} - k_j}\Bigg)B_{j,d-1}(t)$$

Let's assume that $i = s+d$ is the index of the non zero interval in the knot set. Then $i-d = (s+d) - d = s$ and thus the above can be rewritten as:

$$\sum^{m}_{j=0}B_{i,d}(t) = \sum_{j=0}^{d}  \Bigg(\frac{k_{i+j}-t}{k_{i+j}-k_{i-d+j}} + \frac{t-k_{i-d+j}}{k_{i+j} - k_{i-d+j}}\Bigg)B_{i,d-1}(t)$$

Thus:

$$\sum^{m}_{j=0}c_j\cdot B_{i,d}(t) = \sum_{j=0}^{d}  \Bigg(c_j\cdot\frac{k_{i+j}-t}{k_{i+j}-k_{i-d+j}} + c_{j+1}\cdot\frac{t-k_{i-d+j}}{k_{i+j} - k_{i-d+j}}\Bigg)B_{i,d-1}(t)$$

Each term in that sum is a linear interpolation from $c_j$ to $c_{j+1}$. Let's label the interpolated points $c^1_{i} = lerp(c_i, c_{i+1})$.

Then the sum becomes:

$$\sum^{m}_{j=0}c_j\cdot B_{i,d}(t) = \sum_{j=0}^{d}  c^1_j\cdot B_{i,d-1}(t)$$

Once again we can express the sum of the functions of degree  $d-1$ as:

$$\sum^{m}_{j=0}B_{i,d-1}(t) = \sum_{j=s}^{s+d-1}  \Bigg(\frac{k_{j+d-1}-t}{k_{j+d-1}-k_{j}} + \frac{t-k_j}{k_{j+d-1} - k_j}\Bigg)B_{j,d-2}(t)$$

And thus, since $i=s+d$ then $s+d-1 = i-1$, and since the first non zero basis of degree $d-1$ is the basis with index $s+d-1$:

$$\sum^{m}_{j=0}c^1_j\cdot B_{i,d-1}(t) = \sum_{j=0}^{d-1}  \Bigg(c^1_j\cdot\frac{k_{i-1+j}-t}{k_{i-1+j}-k_{i-d+j}} + c^1_{j+1}\cdot\frac{t-k_{i-d+j}}{k_{i-1+j} - k_{i-d+j}}\Bigg)B_{i,d-2}(t)$$

If we replace the interpolating functions with the resulting points $\{c^2_i\}$ we get:

$$\sum^{m}_{j=0}c_j\cdot B_{i,d-1}(t) = \sum_{j=0}^{d}  c^2_j\cdot B_{i,d-2}(t)$$

And from this it isn't hard to see that in general:

$$\sum^{m}_{j=0}c^l_j\cdot B_{i,d-l}(t) = \sum_{j=0}^{d-l}  \Bigg(c^l_j\cdot\frac{k_{i-l+j}-t}{k_{i-l+j}-k_{i-d+j}} + c^1_{j+1}\cdot\frac{t-k_{i-d+j}}{k_{i-l+j} - k_{i-d+j}}\Bigg)B_{i,d-2}(t)$$

Thus DeBoor's algorithm naturally arises from the algebraic structure of the definition of of the basis functions. We can compute the final result by starting at the higher order Basis functions and just do linear interpolations of the original control points to generate the final result, just as in the code snipet above.
\\ \\
There are many more properties about splines, and in particular we didn't go much over non standard knot vectors since most of their properties are just natural extensions from the properties of the standard vectors. However, this will hopefully give the reader a good intuition as to how B-Splines work and how to code the algorithm when necessary.

\end{document}