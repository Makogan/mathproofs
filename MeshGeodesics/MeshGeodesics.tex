\documentclass[]{article}

\usepackage[margin=1cm]{geometry}
\pagestyle{empty}

\usepackage{floatrow}
\usepackage{mdframed}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[font={small,it}]{caption}

\usepackage[mode=buildnew]{standalone}% requires -shell-escape
\usepackage{tikz}
\usepackage{csquotes}
\usepackage{graphicx}
\usepackage{float}
\usepackage{xcolor}
\usepackage[hidelinks]{hyperref}
\usepgfplotslibrary{patchplots}
\pgfplotsset{compat = newest}
\usepackage{listings}
\usepackage{xcolor}
\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
}

%opening
\title{}
\author{}

\setlength{\parindent}{0pt}
\setlength{\parskip}{\baselineskip}%

\graphicspath{{images/}}

\begin{document}

This document intends to explain how to approximate geodesics on triangular polyhedra,
in a similar fashion as the orignal MMP Algorithm \cite{mitchell_discrete_1987}.

\section*{Theory}
\subsection*{2 triangle case}
Let's first focus on the the simplest non planar polyhedra.
Consider 2 triangles (not necessarily coplanar) that share an edge. Trivially a rotation
$R$ can be applied to one of the triangles around the shared edge to make them coplanar.
Consider then 2 points $E,S$ each in a different face. In the coplanar projection the
geodesic is merely the straight line connecting the 2 points. So we see that a
polyhedral geodesic is in fact just a polyline that becomes straight if all the faces that
contain it are unfolded onto a single plane.

We could calculate the point on the shared edge at whcih to split the polyline like this:

Given $E',S'$ the orthogonal projections of the points $E,S$ onto the shared edge.
The line $\overline{ES}$ can be divided into 4 segments, $\overline{EE'}$ and
$\overline{SS'}$ which are orthogonal to the shared edge, and $\overline{E'G}$ and
$\overline{GS'}$ which are colinear with it.

This implies that $\overline{EE'}$ and $\overline{SS'}$ are parallel. This gives us 2
similar triangles which means we can use the Thales theorem to establish that
$\widehat{EE'G}$ is a scaled (and reflected) version of $\widehat{GS'S}$.

Thus $\overline{E'G} = k\overline{GS'}$ and $\overline{EE'} = k \overline{SS'}$ for some
constant $k$ and so $k = \frac{\|\overline{EE'}\|}{\|\overline{SS'}\|}$.

\begin{figure}[H]
    \includegraphics[scale=0.6]{path}
\end{figure}

We also know that:
\begin{align*}
    &\overline{E'G} + \overline{GS'} = \overline{E'S'}\\
    \iff &\overline{E'S'} = k \overline{GS'} + \overline{GS'} = (k+1)\overline{GS'}\\
    \iff &\frac{\overline{E'S'}}{(k+1)} = \overline{GS'}
\end{align*}

Let $t = \frac{\|\overline{E'S'}\|}{(k+1)}$ then $G = t \overrightarrow{S'} +
(1-t) \overrightarrow{E'}$. Thus getting the point of intersection of the geodesic
witht the shared edge.

\section*{Algorithm}

The above formulation is fine for a case of only 2 triangles, but once we have more it
becomes more complex. That's what the MMP algorithm is for, it is composed of two main
steps.

The general idea is simple, starting at the face containing a source point, we
precalculate a list of parameters to store at each edge such that we can find
a strip of connected triangles that joins an end point to the source in linear time
(linear in the number of triangles in the strip).

This is possible because by storing the correct information, we can map the problem
locally to a plane. That is to say, just like the above example shows, as long as you
focus only on the intersection of the polyline with the current face you are looking at,
you can unfold all necessary parameters onto the plane of that face and treat the problem
as a classic Euclidena problem within the interior of the face.

\subsection*{Initilization}
Before we start, a few notes on modifications I have made.
In the original algorithm, the interval of optimality holds the
following data \cite{surazhsky_fast_2005}:
\begin{displayquote}
    To summarize, the distance field $D(p)$ over the window is ex-pressed as a tuple
    $(b_0, b_1, d_0, d_1, \sigma, \tau)$ where $b_0$, $b_1$ the end-points of $w$, $d_0$, $d_1$
    are the corresponding distances to the pseudo-source, $\sigma$ the geodesic distance
    from $s$ to the source $v_s$, and $\tau$ encodes the direction of $s$ from the directed
    edge $e$.
\end{displayquote}

I personally decided to change some of it, as I found it easier to work with. Rather
than storing $d_0$ and $d_1$ I store $f$, the distance from the first point in the edge to
the orthogonal projection of $v_s$ onto the edge and $\tau$ is a strictly positive number
encoding the distance from $v_s$ to $f$. Because this algorithm runs on a half edge graph
in my implementation, the direction is implicitly encoded by selecting only one
of the 2 half edges to associate with each interval.

This makes the reconstruction of the pseudo-source simple to calculate.
Given $f, b_0, b_1, \tau, u_0$ (where $u_0$ is the first point of the half edge) and a
plane normal $n$. We simply obtain $v_s$ by reconstructing $v_f, v_{b_0}, v_{b_1}$
(the vertices of defined by $f, b_0, b_1$). Then we compute:

$$v_s = \frac{v_{b_1} - v_{b_0}}{||v_{b_1} - v_{b_0}||} \times n * \tau + v_{f}$$

What can be considered as unprojecting the orthogonal projection from the edge back onto
the source in the planar unfolding of the face.

\subsubsection*{Modified procedure}
The algorithm starts at the source point and creates 3 intervals, one for each pair of
points in the edges of the starting face. A queue is initialized with the three windows of
optimality (which is essentially a 2D cone at the start of the algorithm) and we start
popping from the que, checking for intersections between edges and windows.

\begin{figure}[H]
    \includestandalone[trim=0px 300px -200px 150px, clip, width=0.8\textwidth]{Figures/Start}
    \caption{At the start of the algorithm, we create 3 widnows of optimality, which
        define planar cones that will intersect the edges of the mesh.}
\end{figure}

essentially, a window (a cone) that has already intersected one of the 3 edges of a face
may intersect one or both of the edges opposite to that edge. We must find all the edges
that the window intersect, calculate the points of intersection and then push them onto
the queue as well. We do this until we either hit a boundary or until we find an edge
we already intersected and all over the intersection region the prior intervals of
optimality are closer to their sources than the current one being tested.

\begin{figure}[H]
    \includestandalone[trim=0px 400px -400px 120px, clip,width=1.2\textwidth]{Figures/WindowEdge}
    \caption{The green edge represents the intersection of the window with the edge.}
\end{figure}

When 2 intervals of optimality have a non null intersection, we must move the endpoints of
each interval such that the intersection becomes a single point and each point in each interval
is closer to its source than to that of its neighbour.

\begin{figure}[H]
    \includestandalone[trim=0px 400px -400px 120px, clip,width=1.2\textwidth]{Figures/WindowOverlap}
    \caption{The mediatrice $fm$ splits the segment into 2 regions.}
    \label{fig:overlap}
\end{figure}

The original paper suggests solving a quadratic equation. I attempted a more geometric
approach. In ~\ref{fig:overlap} the line $\overline{fm}$ is the mediatrice of the
line connecting the 2 sources $o_s, n_s$. By definition then, the point $f$ splits the
line into 2 segments, one where all the points are closer to $o_s$ and one where they are
closer to $n_s$. Thus when solving an interval overlap we can just use the point $f$ as
reference and move the boundaries of the intervals such that their intersection is
exactly $\{f\}$ but their union remains the same as it was before readjusting the
boundaries. The intersection of the mediatrice is not guaranteed to overlap with the
uinion of the original itnervals, in these cases we simply discard the interval with the
farthest source from the edge, as there is no point on that interval that
satisfies the requirements.

In short, using the mediatrice provides an equivalent test as that described in the paper,
but it might be easier to implement, or at minimum the author finds it more intuitive.

% REFERENCES =============================================================================
\newpage
\bibliography{bibliography}
\bibliographystyle{ieeetr}

\end{document}
