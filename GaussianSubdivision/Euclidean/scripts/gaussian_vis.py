import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import scipy.linalg as la
import math
from colour import Color
from fontTools.ttLib import TTFont
import matplotlib.font_manager as mfm
import argparse
import os


def MakeParser():
    dir = os.path.dirname(__file__)
    parser = argparse.ArgumentParser(description='Generate gaussian subdivision figures')
    parser.add_argument('--path', default=os.path.join(dir, 'data.txt'),
                        help='Path to the data file')
    parser.add_argument('--out', default=os.path.join(dir, 'out.png'),
                        help='Path where to output plot')

    return parser.parse_args()


def char_in_font(Unicode_char, font):
    for cmap in font['cmap'].tables:
        if cmap.isUnicode():
            if ord(Unicode_char) in cmap.cmap:
                return True
    return False

def EvaluateGaussianProduct(mu1, sigma1, alpha1, mu2, sigma2, alpha2):
    sigma = np.linalg.inv(alpha1 * sigma1 + alpha2 * sigma2)
    mu = sigma * (alpha1 * sigma1 * mu1 + alpha2 * sigma2 * mu2)

    return (np.array(mu), sigma)


def GaussianCurve(mu1, sigma1, mu2, sigma2):
    curve = []
    for i in range(0, 100):
        t = float(i) / 100.0
        point, cov = EvaluateGaussianProduct(
            mu1, sigma1, (1.0 - t), mu2, sigma2, t)
        curve.append(np.array(point))

    return curve


def GetPoints(curve):
    x = []
    y = []
    for point in curve:
        x.append(point[0][0])
        y.append(point[1][0])

    return x, y


def CreateGaussianEllipse(mu, sigma, scale=1, color='#aaaaff', fill=True, linestyle='-'):
    eig_values, eig_vectors = la.eig(sigma)
    eig_values = eig_values.real

    P = np.matrix(eig_vectors)
    rotation_matrix = np.array(P)

    angle = math.acos(rotation_matrix[0][0]) * 360
    ellipse = Ellipse(mu, scale / math.sqrt(eig_values[0]), scale / math.sqrt(eig_values[1]),
        angle=angle, color=color, fill=fill, linestyle=linestyle, linewidth=2.0)

    return ellipse


def CreateLinearColorGradient(color1, color2, n=10):
    colors = []
    c1 = np.array(color1.rgb)
    c2 = np.array(color2.rgb)
    for i in range(n):
        t = float(i) / float(n-1)
        new_color = (1.0 - t) * c1 + t * c2
        colors.append(Color(rgb=new_color))

    return colors


def CreateGaussianIsoContours(mu, sigma, ax):
    samples = 10
    colors = CreateLinearColorGradient(Color('#ffffff'), Color('#6666ff'), samples)

    i = 0
    for color in colors:
        scale = (1.0 -( float(i) / float(samples - 1))) * 1.5
        ellipse = CreateGaussianEllipse(mu, sigma, scale, color.hex_l)
        ax.add_patch(ellipse)
        i+=1


def GetGaussianParameters(string):
    tokens = string.split(';')
    dictionary = dict()
    for token in tokens:
        pair = token.split('=')
        pair[0] = pair[0].strip()
        pair[1] = pair[1].strip()

        dictionary[pair[0]] = pair[1]

    for mu in ['mu1', 'mu2']:
        string_values = dictionary[mu].split(',')
        values = [(float(string_values[0]), float(string_values[1]))]
        dictionary[mu] = np.transpose(np.array(values))

    for sigma in ['sigma1', 'sigma2']:
        string_values = dictionary[sigma].split(',')
        values = \
            ((float(string_values[0]), float(string_values[1])),
            (float(string_values[2]), float(string_values[3])))
        dictionary[sigma] = np.matrix(values)

    dictionary['t'] = float(dictionary['t'])

    return (dictionary['mu1'],
            dictionary['mu2'],
            dictionary['sigma1'],
            dictionary['sigma2'],
            dictionary['t'])

def VectorScaling(vector, scaling):
    result = (scaling - 1) * vector.transpose() * vector + np.identity(vector.shape[1])
    return np.array(result)

def DrawParametricCurve():
    args = MakeParser()
    with open(args.path, 'r') as f:
        content = f.read()
    mu1, mu2, sigma1, sigma2, t = GetGaussianParameters(content)

    sigma1 = VectorScaling(np.matrix([1, 1]), 300).dot(sigma1)
    sigma2 = VectorScaling(np.matrix([1, 1]), 300).dot(sigma2)

    for i in range(1):
        t = float(i) / 10.0
        # Generate Gaussian interpolating curve
        curve = GaussianCurve(mu1, sigma1, mu2, sigma2)

        # Start the plot by plotting the color gradients of the endpoints
        fig, ax = plt.subplots(figsize=(6, 6))
        plt.axis('off')
        plt.xlim((-2.0, 2.0))
        plt.ylim((-2.0, 2.0))
        CreateGaussianIsoContours(mu1, sigma1, ax)
        CreateGaussianIsoContours(mu2, sigma2, ax)

        # Plot the curve on top
        x, y = GetPoints(curve)
        ax.plot(x, y,  color='#aa3333', linewidth=2.0)

        # Mark the endpoints
        ellipse = Ellipse(mu1, 0.1, 0.1, color='black', zorder=6)
        ax.add_patch(ellipse)
        ellipse = Ellipse(mu2, 0.1, 0.1, color='black', zorder=6)
        ax.add_patch(ellipse)

        label = r'$\mu_i$'
        ax.text(mu1[0] - 0.1, mu1[1] + 0.19, label, fontsize=20)
        label = r'$\mu_j$'
        ax.text(mu2[0] + 0.1, mu2[1] + 0.19, label, fontsize=20)

        # Mark key interpolating points along the curve
        point, cov = EvaluateGaussianProduct(
            mu1, sigma1, 1.0 - t, mu2, sigma2, t)
        ellipse = Ellipse(point, 0.1, 0.1, color='black', zorder=6)
        ax.add_patch(ellipse)
        ellipse = CreateGaussianEllipse(point, cov, 0.5, '#aa3333', linestyle='--', fill=False)
        ax.add_patch(ellipse)

        label = r'$\mu_{ij}$'
        ax.text(point[0], point[1] + 0.34, label, fontsize=20)

        plt.savefig(args.out)
        plt.show()


if( __name__ == "__main__" ):
    DrawParametricCurve()