import numpy as np
from pyx import *
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve
import os
from IPython.display import SVG
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def VectorScaling(vector, scaling):
    result = (scaling - 1) * vector.transpose() * vector + np.identity(vector.shape[1])
    return np.array(result)


points = []

sigma1 = np.array([
    [1, 0.3],
    [0.3, 1]
])

sigma2 = np.array([
    [1, 0],
    [0, 1]
])

ran = 20
for i in range(ran):
    for j in range(ran):
        points.append((i, j))

sigma3 = VectorScaling(np.matrix([1,1]), 3)

c = canvas.canvas()
for p in points:
    mod_point1 = sigma1.dot(np.array([p[0], p[1]]))
    #c.fill(path.circle(mod_point1[0], mod_point1[1], 0.5), [color.rgb.red])
    mod_point2 = sigma2.dot(np.array(p))
    #c.fill(path.circle(mod_point2[0], mod_point2[1], 0.1), [color.rgb.green])
    mod_point3 = sigma3.dot(np.array(p))
    c.fill(path.circle(mod_point3[0], mod_point3[1], 0.5), [color.rgb.blue])

c.writeSVGfile(os.path.join(os.path.dirname(__file__), "metapost"))
