\documentclass[]{article}

\usepackage[margin=1cm]{geometry}
\pagestyle{empty}

\usepackage{floatrow}
\usepackage{mdframed}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[font={small,it}]{caption}

\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[hidelinks]{hyperref}
\usepgfplotslibrary{patchplots}
\pgfplotsset{compat = newest}
\usepackage{listings}
\usepackage{xcolor}
\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
}

%opening
\title{}
\author{}

\setlength{\parindent}{0pt}
\setlength{\parskip}{\baselineskip}%

\graphicspath{
	{images/}
}

\begin{document}

Gaussian Subdivision Surfaces are an interesting way to think about shape modelling
created by Dr. Reinhold Preiner and published in SIGGRAPH 2019. I
intend to convey as much of their intuition as possible in this set of notes,
both from a theoretical perspective, as well as an implementation approach.

\section*{Extending Points to Gaussian PDFs}

The first thing to understand is a generalization of points into multidimensional
Gaussian Probability Density Functions (PDFs).

We usually think of points as a singular, infinitesimally small, section of a
multidimensional space; a point is, in a way a 0D manifold. This can be extended to the
idea that the point is the mean of a multivariate Gaussian PDF.

That's perhaps a bit heavy so let's see what this actually means. Lets consider some
arbitrary Gaussian PDFs:

\begin{tikzpicture}
\begin{axis}[]
\addplot[
samples = 200,
smooth,
ultra thick,
blue,
] {exp((-1/2) * (x)^2 / 1)};
\end{axis}
\end{tikzpicture}
\begin{tikzpicture}
\begin{axis}[]
\addplot[
samples = 200,
smooth,
ultra thick,
blue,
] {exp((-1/2) * (x)^2 / 4)};
\end{axis}
\end{tikzpicture}


Both functions have a mean and a variance. The mean determines the "position" of the curve
and the variance determines the "shape". The curve in the left has a smaller variance than
 that on the right, making it "narrower".

If we jump to 2D we get the following:

\begin{center}
\begin{figure}[h!]
	\captionsetup{labelformat=empty}
	\caption{Images displaying an anisotropic 2D Gaussian distribution}
	\includegraphics[width=1\textwidth]{GaussianVis}
\end{figure}
\end{center}

Just like in the 1D case, the 2D version also has a mean, which is the center of the
elliptical region (the highest point in the 2D curve). However, the 2D version has a
generalisation of the variance called the covariance matrix, which is what determines the
final shape of the distribution, just like the variance did for the 1D case.

Thus the generalisation of points into Gaussian PDFs is merely extending the position
vector $\mu = <x_1, x_2, \dots, x_n>$ with a covariance matrix $\Sigma$, where:

$$
\Sigma=
\begin{bmatrix}
cov(x_1, x_1) & cov(x_1, x_2) & \dots & cov(x_1, x_n)\\
cov(x_2, x_1) & cov(x_2, x_2) & \dots & cov(x_2, x_n)\\
\vdots & \vdots &\ddots \\
cov(x_n, x_1) & cov(x_n, x_2) & \dots & cov(x_n, x_n)\\
\end{bmatrix}
$$

And since $cov(x_i, x_j) = cov(x_j, x_i)$, $\Sigma$ is a symmetric matrix.

For the 2D case when the norm of the covariance matrix is small the "hill" is tall and
narrow, if the norm becomes larger, the "hill" becomes shorter and wider.
The relative differences between the entries in the covariance
matrix also affect the anisotropy of the distribution, for example a large $cov(x,x)$
entry and a small $cov(y,y)$ entry with all other terms being 0 will look like a narrow
horizontal ellipse.
And thus a Gaussian point $\Theta$ is defined as: $\Theta = \{\mu, \Sigma\}$.

Finally, perhaps the most important definiton is that a product of Gaussian PDFs results in
the following mean and covariance matrix:

\begin{mdframed}[backgroundcolor=blue!20]
	\begin{align*}
		\mu_{J} = \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)
		& &\Sigma_{J} = \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}
	\end{align*}
\end{mdframed}

The proof of this fact is given in the
\color{blue}
\hyperref[sec:productGaussians]{product of Gaussian PDFs section.}
\color{black}

% NEW SECTION ============================================================================
\section*{Non linear curve with only 2 points}

When we only have 2 Gaussian points, we can simplify the mean of the product of Gaussian
PDFs to:

\begin{align*}
	\mu_{ij} = \Big(\alpha_i\Sigma^{-1}_i + \alpha_j\Sigma^{-1}_j\Big)^{-1}
		\Big(\alpha_i\Sigma_i^{-1}\mu_i + \alpha_j\Sigma_j^{-1}\mu_j\Big)
\end{align*}


We can then define the following variables:

\begin{align*}
	t = \frac{\alpha_i}{\alpha_i + \alpha_j} & & s = \alpha_i + \alpha_j
\end{align*}

Thus we get that $\alpha_i = st$ and $\alpha_j = s(1-t)$ And thus we can write:

\begin{align*}
	\mu_{ij} &= \Big(st\Sigma^{-1}_i + s(1-t)\Sigma^{-1}_j\Big)^{-1}
		\Big(st\Sigma_i^{-1}\mu_i + s(1-t)\Sigma_j^{-1}\mu_j\Big) \\
		&= \Big(t\Sigma^{-1}_i + (1-t)\Sigma^{-1}_j\Big)^{-1}
		\Big(t\Sigma_i^{-1}\mu_i + (1-t)\Sigma_j^{-1}\mu_j\Big) \\
\end{align*}

And thus by simplifying $s$ we get a parametrization of a curve between $\Theta_i$ and
$\Theta_j$.

A curious property about this curve is that, although at first glance it looks like linear
interpolations, it is not always the case. When the covariance matrices of the end points
are different, the curve can end up looking somewhat like a bezier curve. As $t$ goes from
0 to 1, the resulting interpolated point's covariance matrix changes shape, as it is also
interpolated by the operation. An example of this is visible in this figure:

\begin{center}
\begin{figure}[h!]
	\captionsetup{labelformat=empty}
	\caption{Evolution of an interpolated Gaussian point. The red curve is the
	interpolation curve. The red dotted line is an isocontour of the interpolated point's
	PDF. The blue regions are some isocontours of the Gaussian PDFs of the end points.}
	\includegraphics[width=0.4\textwidth]{gaussian1}
	\includegraphics[width=0.4\textwidth]{gaussian2}
	\includegraphics[width=0.4\textwidth]{gaussian3}
	\includegraphics[width=0.4\textwidth]{gaussian4}
\end{figure}
\end{center}

Modifying the the entries of the covariance matrices of the end points will affect the
final shape of the curve. However it's not completely intuitive how to predict the resulting
curve. None the less we can get at least some intuition by understanding how
the Gaussian properties describe the ellipsoidal isocontours. Which will be explained in the
\color{blue}
\hyperref[sec:gaussianIsocontours]{Gaussian Isocontours section.}
\color{black}

% NEW SECTION ============================================================================
\section*{Creating a Dual Space}
\label{sec:creatingDualSpace}

We could just use the derived formulas to do our calculations, however the original paper
 provides a way to linearize the Gaussian representation to simplify doing Subdivision
(or any other opertaion relying on the Gaussian product).

We start by defining the half vectorization $vech$ operator which linearizes the lower
half of a symmetric matrix. In other words if we define the symmetric matrix
$\Sigma = [\phi_{ij}]$ with dimensions $n\times n$:

$$
vech(\Sigma) = [v_{k}] \text{ where } k = \frac{i(i+1)}{2} + j \text{; with }
0 \leq j \leq i < n \text{ and } v_k=\phi_{ij} \in \Sigma
$$

With that defined, let's analyze the exponent of a multivariate Gaussian PDF.

\begin{align*}
(X-\mu)^T\Sigma^{-1}(X-\mu) &= (X-\mu)^T(\Sigma^{-1}X-\Sigma^{-1}\mu) \\
&= X^T\Sigma^{-1}X - X^T\Sigma^{-1}\mu - \mu^T\Sigma^{-1}X + \mu^T\Sigma^{-1}\mu
\end{align*}

$\mu^T\Sigma^{-1}X$ is trivially a scalar, so it is equal to its transpose.
Since $\Sigma^{-1}$ is symmetric:

$$
\mu^T\Sigma^{-1}X = \Big(\mu^T\Sigma^{-1}X\Big)^T = X^T\Sigma^{-1}\mu
$$

And thus we can rewrite the above equation as:

$$
(X-\mu)^T\Sigma^{-1}(X-\mu) = X^T\Sigma^{-1}X - 2X^T\Sigma^{-1}\mu + \mu^T\Sigma^{-1}\mu
$$

We get:

\begin{align*}
	\Sigma^{-1}X &=
	\begin{bmatrix}
		\sum^{n-1}_{i=0}\phi_{0,i}x_{i} \\
		\sum^{n-1}_{i=0}\phi_{1,i}x_{i} \\
		\vdots \\
		\sum^{n-1}_{i=0}\phi_{n-1,i}x_{i}
	\end{bmatrix}
	&\implies& &
	X^T\Sigma^{-1}X &=
	\sum^{n-1}_{i=0}\sum^{n-1}_{j=0}\phi_{i,j}x_{j}x_{i}
\end{align*}

However we know that for $i\neq j$, $\phi_{i,j} = \phi_{j,i}$, so the above sums can
be rewritten as:

\begin{align*}
	X^T\Sigma^{-1}X &=
	\sum^{n-1}_{i=0}\sum^{n-1}_{j=0}\phi_{i,j}x_{j}x_{i} \\
	&=
	\sum^{n-1}_{i=0}\sum^{i-1}_{j=0}2\phi_{i,j}x_{j}x_{i}
		+ \sum^{n-1}_{i=0} \phi_{i,i}x_i^2
\end{align*}

Note that because the term $\mu^T \Sigma^{-1} \mu$ is a constant, it can be absorbed
by the $C$ factor outside of the exponent, thus we can ignore it and we get:

\begin{align*}
(X-\mu)^T\Sigma^{-1}(X-\mu) - \mu^T\Sigma^{-1}\mu &= X^T\Sigma^{-1}X - 2X^T\Sigma^{-1}\mu \\
&= \Bigg(\sum^{n-1}_{i=0}\sum^{i-1}_{j=0}2\phi_{i,j}x_{j}x_{i}
	+ \sum^{n-1}_{i=0} \phi_{i,i}x_i^2\Bigg) - 2X^T\Sigma^{-1}\mu
\end{align*}

Let's now define the following polynomial basis:

$$
\textbf{b}(X) = (vech(2XX^T - diag(X)^2), -2X)
$$

Where $diag(X)$ is a diagonal matrix where the diagonal entries are the components of $X$.

Analyzing every term gives:

\begin{align*}
	XX^T &=
	\begin{bmatrix}
		x_0 \\
		x_1 \\
		\vdots \\
		x_{n-1}
	\end{bmatrix}
	\begin{bmatrix}
		x_0 & x_1 & \dots & x_{n-1} \\
	\end{bmatrix}
	=
	\begin{bmatrix}
		x_0x_0 & x_0x_1 &\dots &x_0x_{n-1} \\
		x_1x_0 & x_1x_1 &\dots &x_1x_{n-1} \\
		\vdots & \vdots &\ddots &\vdots \\
		x_{n-1}x_0 & x_{n-1}x_1 &\dots &x_{n-1}x_{n-1}
	\end{bmatrix} \\
	\\
	\implies 2XX^T - diag(X)^2 &=
	\begin{bmatrix}
		x_0^2 & 2x_0x_1 &\dots &2x_0x_{n-1} \\
		2x_1x_0 & x_1^2 &\dots &2x_1x_{n-1} \\
		\vdots & \vdots &\ddots &\vdots \\
		2x_{n-1}x_0 & 2x_{n-1}x_1 &\dots &x_{n-1}^2
	\end{bmatrix} \\
	\\
	\implies vech(2XX^T - diag(X^2)) &=
	\begin{bmatrix}
		x^2_0 & 2x_1x_0 & x_1^2 & 2x_2x_0 & 2x_2x_1 & x_2^2 &\dots &2x_{n-1}x_{n-2} &x_{n-1}^2
	\end{bmatrix}
\end{align*}

Now let's define the coefficient vector:
$Q = (\breve{Q}, \bar{Q})$. Where:

\begin{mdframed}[backgroundcolor=blue!20]
\begin{align*}
	\breve{Q} &= vech(\Sigma^{-1}) & \bar{Q}&=\Sigma^{-1}\mu\\
\end{align*}
\end{mdframed}

And thus:

\begin{align*}
	\breve{Q} &= vech(\Sigma^{-1})
	=
	\begin{bmatrix}
		\phi_{0,0} &\phi_{1,0} &\phi_{1,1} &\phi_{2,0} &\dots &\phi_{n-1, n-1}
	\end{bmatrix}
\end{align*}

And thus the product $\textbf{b}(X)^TQ$ is:
\begin{align*}
	\textbf{b}(X)^TQ = (vech(2XX^T - diag(X)^2), -2X)^T(\breve{Q}, \bar{Q}) \\
\end{align*}

Computing each portion separately:

\begin{align*}
	vech(2XX^T - diag(X)^2)^T\breve{Q} &=
	\begin{bmatrix}
		x^2_0 \\
		2x_1x_0 \\
		\vdots \\
		x_{n-1}^2
	\end{bmatrix}
	\begin{bmatrix}
		\phi_{0,0} &\phi_{1,0} &\dots &\phi_{n-1, n-1}
	\end{bmatrix}
	& &-2X^T\bar{Q} = -2X^T\Sigma^{-1}\mu \\
	\\
	&= \sum^{n-1}_{i=0}\sum^{i-1}_{j=0} 2\phi_{i,j}x_jx_i + \sum^{n-1}_{i=0}\phi_{i,i}x_i^2
\end{align*}

And finally:
$$
\textbf{b}(X)^TQ = \Bigg(\sum^{n-1}_{i=0}\sum^{i-1}_{j=0} \phi{i,j}x_jx_i
	+ \sum^{n-1}_{i=0}x_i^2 \Bigg)
	-2X^T\Sigma^{-1}\mu
$$

Henceforth:
$$
\textbf{b}(X)^TQ = (X-\mu)^T\Sigma^{-1}(X-\mu) - \mu^T\Sigma^{-1}\mu
$$

Since we are not concerned about constant terms let's use $\approx$ to denote expressions
that are only different by constant terms:

\begin{mdframed}[backgroundcolor=blue!20]
$$
\textbf{b}(X)^TQ \approx (X-\mu)^T\Sigma^{-1}(X-\mu)
$$
\end{mdframed}

% NEW SECTION ============================================================================
\section*{Product of Gaussian PDFs}
\label{sec:productGaussians}

\textit{This section is optional. If you understand everything so far
and are familiar with multivariate Gaussian PDFs just jump to
\color{blue}
\hyperref[sec:usinDualSpace]{Using the Dual Space.}
\color{black}
}

The definition of the multivariate Gaussian PDF associated with the Gaussian point
$\Theta = \{\mu, \Sigma\}$ is as follows:

$$
|2\pi\Sigma|^{-\frac{1}{2}} e^{-\frac{1}{2}(x-\mu)^T\Sigma^{-1}(x-\mu)}
$$

However, we are primarily interested in the kernel; as such we will define it as:

$$
C e^{-\frac{1}{2}(X-\mu)^T\Sigma^{-1}(X-\mu)}
$$

And we will let $C$ collect all constant terms in the formula, since we will only focus on
how the mean and the covariance matrix evolve through the transformations we will apply.
The first and most important of such transformations occurs when we take the product of
the powers of multiple Gaussian PDFs.

Let $G = \{f(X|\Theta_i)^{\alpha_i}\}$ be a set of Gaussian PDFs raised to arbitrary
powers. Then:

\begin{equation*}
	\begin{split}
		f(X|\Theta_J) = \prod^n_{i=1} f(X|\Theta_i)^{\alpha_i}
		&= \prod^n_{i=1} C e^{-\frac{1}{2}\alpha_i(X-\mu_i)^T\Sigma_i^{-1}(X-\mu_i)} \\
%
		&= C e^{-\frac{1}{2}\big(\sum^n_{i=1}\alpha_i(X-\mu_i)^T\Sigma_i^{-1}(X-\mu_i)\big)}
	\end{split}
\end{equation*}

Then if we focus only on the exponent and ignore the $-\frac{1}{2}$ constant,
let's define:
$$
L(X) = \sum^n_{i=1}\alpha_i(X-\mu_i)^T\Sigma_i^{-1}(X-\mu_i)
$$
Such that $f(X|\Theta_J) = Ce^{-\frac{1}{2}L(X)}$

Now to simplify $L(X)$:

\begin{equation*}
	\begin{split}
		L(X) &= \sum^n_{i=1}\alpha_i(X-\mu_i)^T\Sigma_i^{-1}(X-\mu_i) \\
		&= \sum^n_{i=1}\alpha_i(X^T\Sigma^{-1}_i - \mu_i^T\Sigma_i^{-1})(X-\mu_i) \\
		&= \sum^n_{i=1}\alpha_i(X^T\Sigma_i^{-1}X - \mu_i^T\Sigma_i^{-1}X - X^T\Sigma_i^{-1}\mu_i +
			\mu_i^T\Sigma_i^{-1}\mu_i) \\
		&= \sum^n_{i=1}\alpha_i(X^T\Sigma_i^{-1}X - \mu_i^T\Sigma_i^{-1}X - X^T\Sigma_i^{-1}\mu_i) +
		\sum^n_{i=1}\mu_i^T\Sigma_i^{-1}\mu_i
	\end{split}
\end{equation*}

Let's note that the term $\sum^n_{i=1}\mu_i^T\Sigma_i^{-1}\mu_i$ is constant
and thus can be absorbed by the $C$ term in the first formula. In other words,
let's define:

$L'(X) = L(X) - \sum^n_{i=1}\mu_i^T\Sigma_i^{-1}\mu_i$

Thus $Ce^{-\frac{1}{2}L(X)} = C'e^{-\frac{1}{2}L'(X)}$. We will however abuse notation and
replace $C'$ with $C$ and $L'(X)$ with $L(X)$. Thus we can simplify the above and write:

\begin{align*}
	L(X) &= \sum^n_{i=1}\alpha_i(X^T\Sigma_i^{-1}X - \mu_i^T\Sigma_i^{-1}X
		- X^T\Sigma_i^{-1}\mu_i) \\
	&=  \sum^n_{i=1}\alpha_i X^T\Sigma_i^{-1}X
		- \sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}X
		- \sum^n_{i=1}\alpha_i X^T\Sigma_i^{-1}\mu_i \\
	&= X^T\sum^n_{i=1}(\alpha_i\Sigma^{-1}_iX
		- \alpha_i\Sigma_i^{-1}\mu_i)
		- \sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}X \\
	&= X^T\Big(\big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\big)X
		- \sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)
		- \Big(\sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}\Big)X \\
	&= X^T\Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)\Big(X
		- \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)\Big)
		- \Big(\sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}\Big)X
\end{align*}


Let's focus on the {term $\Big(\sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}\Big)X$.
Since covariance matrices are by definition symmetric:

$$
\Big(\sum^n_{i=1}\alpha_i \mu_i^T\Sigma_i^{-1}\Big)X =
\Big(\sum^n_{i=1}(\alpha_i\Sigma_i^{-1}\mu_i)^T\Big)X =
\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)^TX
$$


Then we have:
\begin{equation*}
	\begin{split}
	L(X) &= X^T\Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)\Big(X
	- \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)\Big)
	- \Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)^TX \\
	&= X^T\Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)\Big(X
	- \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)\Big)
	- \Bigg(\Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)\Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)\Bigg)^TX
	\end{split}
\end{equation*}

We will then define two new symbols:

$\mu_{J} = \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}\Big(\sum^n_{i=1}\alpha_i\Sigma_i^{-1}\mu_i\Big)$

$\Sigma_{J} = \Big(\sum^n_{i=1}\alpha_i\Sigma^{-1}_i\Big)^{-1}$

Note that $\Sigma_{J}$ is a symmetric matrix since it's just a sum of scalar products of
symmetric matrices. With this we can rewrite the above as:

\begin{equation*}
	\begin{split}
	L(X) &= X^T\Sigma_{J}^{-1}(X
		- \mu_{J}) - (\Sigma_{J}^{-1}\mu_{J})^TX
	\end{split}
\end{equation*}

We are allowed to introduce constant terms into this expression, since this
just affects the $C$ coefficient of the original formula, we can once again abuse notation
and get:

\begin{equation*}
	\begin{split}
	L(X) &= X^T\Sigma_{J}^{-1}(X - \mu_{J}) - \mu_{J}^T\Sigma_{J}^{-1}X
		- \mu_{J}^T\Sigma_{J}^{-1}\mu_{J} \\
		&= X^T\Sigma_{J}^{-1}(X - \mu_{J}) - \mu_{J}^T\Sigma_{J}^{-1}
		(X- \mu_{J}) \\
		&= (X^T - \mu_{J}^T)\Sigma_{J}^{-1}(X- \mu_{J}) \\
		&= (X - \mu_{J})^T\Sigma_{J}^{-1}(X- \mu_{J})
	\end{split}
\end{equation*}

And thus we get a new Gaussian PDF with mean $\mu_{J}$ and covariance matrix $\Sigma_{J}$.

% NEW SECTION ============================================================================
\section*{Using the Dual Space}
\label{sec:usinDualSpace}

From the
\color{blue}
\hyperref[sec:creatingDualSpace]{Creating Dual Space section.}
\color{black}
we get that:

\begin{align*}
	\underset{\textit{\tiny{$C$ absorbs all constants in the exponent}}}
	{Ce^{-\frac{1}{2}(X-\mu)^T\Sigma^{-1}(X-\mu)}
	=
	Ce^{-\frac{1}{2}\textbf{b}(X)^TQ}}
\end{align*}

Thus if we are given 2 Gaussian points $\Theta_i$, $\Theta_j$, we can define their
associated linearized coefficient vectors $Q_i, Q_j$ and get:

\begin{align*}
	f(X|\Theta_i)^{\alpha_i}f(X|\Theta_j)^{\alpha_j}
	&=
	Ce^{-\frac{1}{2}(\alpha_i(X-\mu_i)^T\Sigma_i^{-1}(X-\mu_i)
		+ \alpha_j(X-\mu_j)^T\Sigma_j^{-1}(X-\mu_j))} \\
	&=Ce^{\alpha_i\textbf{b}(X)^TQ_i + \alpha_j\textbf{b}(X)^TQ_j} \\
	&=Ce^{\textbf{b}(X)^T(\alpha_iQ_i + \alpha_jQ_j)}
\end{align*}

We see then that with the representation of the exponent described in
\color{blue}
\hyperref[sec:creatingDualSpace]{Creating the Dual Space}
\color{black}
a product of powers of Gaussian PDFs is just a linear combination of their vectorized
representation. And it's trivial to generalize to an arbitrary number of factors.

Henceforth we can take an arbitrary number of Gaussian PDFs, take their product and obtain
the resulting linear combination:

\begin{align*}
	Q_I &= \sum_{I}\alpha_iQ_i \\
	\implies \prod_If(X|\Theta_i) &= Ce^{\textbf{b}(X)^TQ_I}
\end{align*}

Which is a new Gaussian PDF, all that's left is delinearizing $Q_I$ back into a mean and
covariance representation.

We first can restore the covariance matrix of any $Q$ vector by taking its $\breve{Q}$
component and then using its entries to construct the lower half of the covariance matrix.
Take the $k^{th}$  entry in the vector, it's position in the covariance matrix is described
by the function $h$ such that:

\begin{align*}
	h(k) = <f(k), k - \frac{f(k)(f(k) + 1)}{2}> & & f(k) = \bigg\lfloor{\frac{\sqrt{1 + 8k} - 1}{2}}\bigg\rfloor
\end{align*}

And so we have $\phi_{h(k)} = Q_i$ for all rows $r$ and columns $c$ of $\Sigma$ where
$c \leq r$ and $h(k) = <c, r>$, other wise $\phi_{r,c} = \phi_{c, r}$. And thus
$$\Sigma = [\phi_{i,j}]^{-1}$$

Which we will denote as $\Sigma = [\breve{Q}]^{-1}$. And now we use this to get the mean
$$\Sigma\bar{Q} = \Sigma\Sigma^{-1}\mu = \mu$$

Finally:

\begin{mdframed}[backgroundcolor=blue!20]
	\begin{align*}
		\Sigma = [\breve{Q}]^{-1} & & \mu = \Sigma\bar{Q}
	\end{align*}
\end{mdframed}

% NEW SECTION ============================================================================
\section*{Gaussian Isocontours}
\label{sec:gaussianIsocontours}

The PDF of a Gaussian distribution is an exponential function where the exponent is:
$$(X-\mu)^T\Sigma^{-1}(X-\mu)$$
As previously defined. Covariance matrices are symmetric and hence diagonalizable so:
$$(X-\mu)^T\Sigma^{-1}(X-\mu)=(X-\mu)^TP^{-1}\Lambda P(X-\mu)$$

Since covariance matrices are symmetric, by the
\color{blue}
\href{http://www.math.jhu.edu/~hhaosu/Teaching/0203SLA/Notes-Ch8_1.pdf}{spectral theorem}
\color{black}
$P$ is an orthogonal matrix and so we get:
$$(X-\mu)^T\Sigma^{-1}(X-\mu)=(X-\mu)^TP^T\Lambda P(X-\mu)=(P(X-\mu))^T\Lambda P(X-\mu)$$

Orthogonal matrices are rotations and reflections. So $P(X-\mu)$ is a new rotated or
reflected vector $X_P$ thus we get $X_P^T\Lambda X_P$.
Since $\Lambda$ is a diagonal matrix we get:

$$
X_P^T\Lambda X_P=
\begin{bmatrix}
	x_1 & x_2 &\dots &x_n\\
\end{bmatrix}
\begin{bmatrix}
\lambda_1 & 0 & \dots & 0\\
0 & \lambda_2 & \dots & 0\\
\vdots & \vdots &\ddots & \vdots\\
0 & 0 & \dots & \lambda_n\\
\end{bmatrix}
\begin{bmatrix}
	x_1 \\
	x_2 \\
	\vdots\\
	x_n\\
\end{bmatrix}
= \sum^n_{i=1}\lambda_i x_i^2
$$

The set of all isocontours of the Gaussian PDF is trivially
$(X-\mu)^T\Sigma^{-1}(X-\mu) = c$. Thus we get:

$$\sum^n_{i=1}\lambda_i x_i^2 = c \iff \sum^n_{i=1}\frac{\lambda_i}{c} x_i^2 = 1$$

Which is the implicit equation of the ellipsoid of dimension $n$. And thus the isocontours
of a Gaussian PDF are the set of ellipsoids $\sum^n_{i=1}\frac{\lambda_i}{c} x_i^2 = 1$.

From this we get that given a covariance matrix $\Sigma$ we can predict the general shape
of its PDF. The eigen values of $\Sigma$ are the coefficients of a multidimensional
ellipsoid and its eigenvectors describe a rotation or reflection matrix applied around the
mean $\mu$ of the PDF. For the purposes of modelling it may be more intuitive to define
the characteristic ellipsoid and rigid transformation first, and use them to construct the
covariance matrix by going backwards through the above process.

\section*{Intuitions and suggestions}
Although it is very interesting to think about this methodology as gaussian distributions
it is not necessary to create a working implementation. Thanks to the dual space
we can just focus on the linear map from object space to dual space:
$\bar{Q} = \Sigma^{-1}\mu$ and its inverse $\mu = \Sigma\bar{Q}$.

Ultimately when we multiply by $\Sigma^{-1}$ all we are doing is applying a rotation,
followed by an axis aligned scaling and then rotating in the oposite direction. This is
putting the vertex $\mu$ in a deformed or "stretched" space.
then we apply a subdivision on dual space, what we are doing
is linearly mix the points in the deformed space as well as their associated deformation
functions. In other words, $\alpha_i\Sigma_i^{-1}\mu_i + \alpha_j\Sigma_j^{-1}\mu_j$
is linearly interpolating the image points and $\alpha_ivech(\Sigma_i^{1}) + \alpha_jvech(\Sigma_j^{1})$
is mixing a space deformation.

When we map back to model space through $\Sigma\bar{Q}$, in general, we don't
fully cancel the deformations induced by the original matrices, hence where the non linear
behaviour of the interpolating curves comes from.

Indeed, to construct a valid covariance matrix we can think of it not as a covariance
matrix but a matrix representing the span of an ellipsoid.

i.e if $E(u,v) = \sum\lambda_ix_i^2 = l$ is the implicit equation of a single
ellipsoid when $l$ is fixed, allowing $l$ to vary is the set of all ellipsoids that are
equal to that ellipsoid up to a scalar multiplication, or said otherwise, it is the set of
all isocontours of an axis aligned Gussian PDF. As we saw before,
the $\lambda$ coefficients can be used as the diagonal entries of
the diagonalization of a convariance matrix, and the factored matrix in the diagonalization
is just a rotation matrix.

Thus for the purposes of modelling, rather than thinking in terms of Probability theory,
we can just associate to each point a rotated ellipsoid $RE$ where $R$ is a rotation matrix
and $E$ is the set of all points in the ellipsoid.

If we assume this ellipsoid is the isocontour with value 1, the covariance
matrix just becomes:

$$\Sigma = (R\Lambda R^T)^{-1}$$

Where the entries of $\Lambda$ are just $\frac{1}{c_i^2}$ and $\{c_i\}$ are the coefficients
of the parametrization of the ellipsoid $E$.

\section*{Implementation}
This may look somewhat hard to implement, however, assuming there already is an existing
implementation of a subdivision algorithm, it's very straight forward.

The following is a possible implementation in C++:
\definecolor{dark_green}{rgb}{0,0.5,0}
\lstset{language=C++,
                basicstyle=\footnotesize,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{dark_green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#}
}
\begin{lstlisting}
void Mesh::GaussianSubdivideMesh(Mesh& mesh)
{
	// Map to dual space
	for(auto& vertex : mesh.vertices)
	{
		MatrixXd position = vertex->GetPosition().row(0);
		Vector3d dual_position;
		dual_position << position(0,0), position(0,1), position(0,2);
		MatrixXd sigma_inv(3,3);
		for(uint k=0; k < 6; k++)
		{
			uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
			uint y = k - x * (x + 1) / 2;

			sigma_inv(x,y) = position(3 + k);
			sigma_inv(y,x) = sigma_inv(x,y);
		}
		dual_position = sigma_inv * dual_position;
		vertex->GetPosition()(0,0) = dual_position(0);
		vertex->GetPosition()(0,1) = dual_position(1);
		vertex->GetPosition()(0,2) = dual_position(2);
	}
	LoopSubdivision(mesh.vertices, mesh.edges, mesh.faces);
	// Map to object space
	for(auto& vertex : mesh.vertices)
	{
		MatrixXd position = vertex->GetPosition().row(0);
		Vector3d dual_position;
		dual_position << position(0), position(1), position(2);
		MatrixXd sigma_inv(3,3);
		for(uint k=0; k < 6; k++)
		{
			uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
			uint y = k - x * (x + 1) / 2;

			sigma_inv(x,y) = position(3 + k);
			sigma_inv(y,x) = sigma_inv(x,y);
		}
		dual_position = sigma_inv.inverse() * dual_position;
		vertex->GetPosition()(0,0) = dual_position(0);
		vertex->GetPosition()(0,1) = dual_position(1);
		vertex->GetPosition()(0,2) = dual_position(2);
	}
}
\end{lstlisting}

And you can get some really pretty shapes:

\begin{center}
	\begin{figure}[h!]
		\captionsetup{labelformat=empty}
		\caption{2 possible results of extending LoopSubdivision with
			GPS}
		\includegraphics[width=0.4\textwidth]{GaussianSubdivision}
		\includegraphics[width=0.4\textwidth]{Gaussian2}
	\end{figure}
\end{center}

% REFERENCES =============================================================================
\newpage
\nocite{*}
\bibliography{bibliography}
\bibliographystyle{ieeetr}

\end{document}
