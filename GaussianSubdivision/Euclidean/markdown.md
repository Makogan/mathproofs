---
---

Gaussian Subdivision Surfaces are an interesting way to think about
shape modelling. I intend to convey as much of their intuition as
possible in this set of notes, both from a theoretical perspective, as
well as an implementation approach.

Extending Points to Gaussian PDFs {#extending-points-to-gaussian-pdfs .unnumbered}
=================================

The first thing to understand is a generalization of points into
multidimensional Gaussian Probability Density Functions (PDFs).

We usually think of points as a singular, infinitesimally small, section
of a multidimensional space, a point is, in a way a 0D manifold. This
can be extended to the idea that the point is the mean of a multivariate
Gaussian PDF.

That’s perhaps a bit heavy so let’s see what this actually means. Lets
consider some arbitrary Gaussian PDFs:\
\

;

;

Both functions have a mean and a variance. The mean determines the
“position” of the curve and the variance determines the “shape”. The
curve in the left has a smaller variance than that on the right, making
it “narrower”.

If we jump to 2D we get the following:

![image](2DGaussian){width="40.00000%"}

Just like in the 1D case, the 2D version also has a mean, which is the
center of the ellipse looking region in green. However, the 2D version
has a generalisation of the variance called the covariance matrix, which
is what determines the final shape of the distribution, just like the
variance did for the 1D case.

Thus the generalisation of points into Gaussian PDFs is merely extending
the position vector $\mu = <x_1, x_2, \dots, x_n>$ with a covariance
matrix $\Sigma$, where:

$$\Sigma=
\begin{bmatrix}
cov(x_1, x_1) & cov(x_1, x_2) & \dots & cov(x_1, x_n)\\
cov(x_2, x_1) & cov(x_2, x_2) & \dots & cov(x_2, x_n)\\
\vdots & \vdots &\ddots \\
cov(x_n, x_1) & cov(x_n, x_2) & \dots & cov(x_n, x_n)\\
\end{bmatrix}$$

And since $cov(x_i, x_j) = cov(x_j, x_i)$, $\Sigma$ is a symmetric
matrix.

And thus a Gaussian point $\Theta$ is defined as:
$\Theta = \{\mu, \Sigma\}$.

Product of Gaussians {#product-of-gaussians .unnumbered}
====================

The definition of the multivariate Gaussian PDF associated with the
Gaussian point $\Theta = \{\mu, \Sigma\}$ is as follows:

$$|2\pi\Sigma|^{-\frac{1}{2}} e^{-\frac{1}{2}(x-\mu)^T\Sigma^{-1}(x-\mu)}$$

However, we are primarily interested in the kernel as such we will
define it as:

$$C e^{-\frac{1}{2}(X-\mu)^T\Sigma^{-1}(X-\mu)}$$

And we will let $C$ collect all constant terms in the formula, since we
will only focus on how the mean and the covariance matrix evolve through
the transformations we will apply. The first and most important of such
transformations occurs when we take the product of the powers of 2
Gaussian PDFs.

Let $f(X|\Theta_i)^{\alpha_i}$ and $f(X|\Theta_j)^{\alpha_j}$ be the
powers of the PDFs of 2 arbitrary Gaussian points. Then:

$$f(X|\Theta_i)^{\alpha_i}f(X|\Theta_j)^{\alpha_j}$$
