\documentclass[]{article}

\usepackage[margin=1cm]{geometry}
\pagestyle{empty}

\usepackage{floatrow}
\usepackage{mdframed}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[font={small,it}]{caption}

\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[hidelinks]{hyperref}
\usepgfplotslibrary{patchplots}
\pgfplotsset{compat = newest}
\usepackage{listings}
\usepackage{xcolor}
\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
}

%opening
\title{}
\author{}

\setlength{\parindent}{0pt}
\setlength{\parskip}{\baselineskip}%

\graphicspath{
	{images/}
}

\begin{document}

\section*{Classic spherical Gaussian}
The original Gaussian Subdivision surfaces \cite{preiner_gaussian-product_2019} can be extended to spherical manifolds in a
fairly straightforward way;

A Spherical Gaussian is defined as:

$G(v; a, \mu, \lambda ) = a e^{\lambda(\mu \cdot v - 1)}$

Where $v$ is a vector variable, $\mu$ is the vector lobe of the distribution, $\lambda$ is
the sharpness of the distribution and $a$ is a normalization factor.

Which implies that a product of powers $\alpha_i$ of Spherical Guassians
$G(v;a_i, \lambda_i, \mu_i)$ is equal to:

\begin{align*}
	\prod_i a_i e^{\alpha_i\lambda_i(\mu_i\cdot v - 1)}
	&= \prod_i a_ie^{\sum_i \alpha_i\lambda_i(\mu_i\cdot v - 1)}\\
	&= Ce^{\sum_i \alpha_i\lambda_i(\mu_i\cdot v - 1)}
\end{align*}

Where $C$ represents all constant values outside the exponent. We can define
$\mu' = \frac{\sum_i \alpha_i\lambda_i\mu_i}{|\sum_i \alpha_i\lambda_i\mu_i|}$ and
$\lambda' = |\sum_i \alpha_i\lambda_i\mu_i|$.
And thus the prior expression becomes:

$$Ce^{\lambda'(\mu'\cdot v - 1) + 1 - \sum_i \alpha_i\lambda_i} = C'e^{\lambda'(\mu'\cdot v - 1)}$$

And thus we get a new spherical Gaussian distribution with lobe $\mu'$ and
sharpness $\lambda'$.

This works, however it should be noted that this particular extenstion of the original paper \cite{preiner_gaussian-product_2019}
reduces to doing a simple weighted average of the lobe vectors. So it's
very straightforward to implement and work with.

\section*{Anisotropic Spherical Gaussians}

As a preface, I did not successfully extend the algorithm to the anisotropic case,
I will merely leave the notes of how far I got in this document.

In 2013 Xu et Al introduced Anisotropic Spherical Gaussians \cite{xu_anisotropic_nodate} defined as:

$$c\max(0,v\cdot z)e^{-\lambda(v\cdot x)^2 - \mu(v\cdot y)^2}$$

Where $x,y,z$ form an orthonromal basis of $\mathbb{R}^3$, $z$ is the lobe of the distribution and $\lambda, \mu$ are parameters controlling the variance of the distribution in 2 different directions (hence inducing anisotropy). The final goal remains to find a linear scheme that allows us to represent the distribution as vectors, such that a product of powers of ASG's reduces to a linear combination of the vectorized representation.

This is what I managed to discover:

For any vector $X$ we have:

$$
\lambda XX^T = \lambda
\left[
\begin{matrix}
	X_{x}^{2} & X_{x} X_{y} & X_{x} X_{z}\\
	X_{x} X_{y} & X_{y}^{2} & X_{y} X_{z}\\
	X_{x} X_{z} & X_{y} X_{z} & X_{z}^{2}
\end{matrix}
\right]
$$

And thus:

\begin{align*}
	\lambda_1 XX^T + \lambda_2 YY^T &= \lambda_1
	\left[
		\begin{matrix}
			X_{x}^{2} & X_{x} X_{y} & X_{x} X_{z}\\
			X_{x} X_{y} & X_{y}^{2} & X_{y} X_{z}\\
			X_{x} X_{z} & X_{y} X_{z} & X_{z}^{2}
		\end{matrix}
	\right]
	+ \lambda_2
	\left[
		\begin{matrix}
			Y_{x}^{2} & Y_{x} Y_{y} & Y_{x} Y_{z}\\
			Y_{x} Y_{y} & Y_{y}^{2} & Y_{y} Y_{z}\\
			Y_{x} Y_{z} & Y_{y} Y_{z} & Y_{z}^{2}
		\end{matrix}
	\right]\\
%
& =
\left[
	\begin{matrix}
		V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1 & V_{x} V_{y} \lambda_2 + X_{x} X_{y} \lambda_1 & V_{x} V_{z} \lambda_2 + X_{x} X_{z} \lambda_1\\
		V_{x} V_{y} \lambda_2 + X_{x} X_{y} \lambda_1 & V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1 & V_{y} V_{z} \lambda_2 + X_{y} X_{z} \lambda_1\\
		V_{x} V_{z} \lambda_2 + X_{x} X_{z} \lambda_1 & V_{y} V_{z} \lambda_2 + X_{y} X_{z} \lambda_1 & V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1
	\end{matrix}
\right]\\
\end{align*}

At this point I was really tempted to say that from this matrix we could extract the following vector:
\begin{align*}
	V=
	\left[
	\begin{matrix}
		(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1)^{1/2}\\
		(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1)^{1/2}\\
		(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1)^{1/2}
	\end{matrix}
	\right]\\
\end{align*}

And then I wanted to show that $VV^T = \lambda_1XX^T + \lambda_2YY^T$ or something close to that, separated just by a constant term or a constant factor. However that product is equal to:

$$
VV^T =
\left[
\begin{matrix}
	\left(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1\right)^{1.0} & \left(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1\right)^{0.5} \left(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1\right)^{0.5} & \left(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1\right)^{0.5} \left(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1\right)^{0.5}\\
	\left(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1\right)^{0.5} \left(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1\right)^{0.5} & \left(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1\right)^{1.0} & \left(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1\right)^{0.5} \left(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1\right)^{0.5}\\
	\left(V_{x}^{2} \lambda_2 + X_{x}^{2} \lambda_1\right)^{0.5} \left(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1\right)^{0.5} & \left(V_{y}^{2} \lambda_2 + X_{y}^{2} \lambda_1\right)^{0.5} \left(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1\right)^{0.5} & \left(V_{z}^{2} \lambda_2 + X_{z}^{2} \lambda_1\right)^{1.0}
\end{matrix}
\right]
$$

Which is very very close. In fact, we can make it even closer. Take:

\begin{align*}
	&\left(V_{i}^{2} \lambda_2 + X_{i}^{2} \lambda_1\right)^{0.5}\left(V_{j}^{2} \lambda_2 + X_{j}^{2} \lambda_1\right)^{0.5} = (V_i^2V_j^2\lambda_2^2 + V_i^2X_j^2\lambda_1\lambda_2 + V_j^2X_i^2\lambda_1\lambda_2 + X_i^2X_j^2\lambda_1^2)^{0.5} \\
	=&
	(V_i^2V_j^2\lambda_2^2 +
	 2V_iV_jX_iX_j\lambda_1\lambda_2 +
	 X_i^2X_j^2\lambda_1^2 +
	 V_i^2X_j^2\lambda_1\lambda_2 +
	 V_j^2X_i^2\lambda_1\lambda_2 -
	 2V_iV_jX_iX_j\lambda_1\lambda_2
	)^{0.5}\\
	=&
	((V_iV_j\lambda_2 + X_iX_j\lambda_1)^2 +
	 V_i^2X_j^2\lambda_1\lambda_2 +
	 V_j^2X_i^2\lambda_1\lambda_2 -
	 2V_iV_jX_iX_j\lambda_1\lambda_2)^{0.5} \\
	=&
	((V_iV_j\lambda_2 + X_iX_j\lambda_1)^2 +
	 \lambda_1\lambda_2(V_iX_j - V_jX_i)^2)^{0.5}
\end{align*}

And here is where I got stuck. If we could eliminate that right term or factor it out of the square root then we would have exactly what we need $VV^T = \lambda_1XX^T + \lambda_2YY^T$.

Under the assumption this can be done, then our dual space is just the vector obtained by concatenating $vech(XX^T), vech(YY^T)$ on the original paper.

Then we can just do any linear combination we want and once we want to map back to the regular space we just take the diagonal of the resulting matrices, square root each element, normalize the 2 resulting vectors and then take their cross product (since the lobe and the resulting vectors must form an orthonromal basis).

Alas this is how far I got.

% REFERENCES =============================================================================
\newpage
\bibliography{bibliography}
\bibliographystyle{ieeetr}

\end{document}
